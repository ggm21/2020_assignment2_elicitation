# Assignment 2 - Elicitation

## Gruppo

- Mirco Malanchini; 829889 (mircomalanchini@hotmai.it)
- Gaetano Magazzù; 829685 (gaetanomagazzu3@gmail.com, lordfighterx@gmail.com)
- Giuseppe Magazzù; 829612 (giuseppe.magazu@gmail.com)

## Descrizione

L'Università di Milano Bicocca offre un servizio di bus navetta gratuito ai propri Studenti e Dipendenti
per gli spostamenti all'interno del Campus Universitario, per le zone di Milano, Monza e Sesto San Giovanni.
Attualmente per consultare gli orari e le fermate della navetta esistono delle indicazioni accessibili in
formato cartaceo ad ogni fermata, ed in formato digitale sul sito dell'ateneo.

Il sistema che si vuole sviluppare consiste in un'applicazione multipiattaforma con
lo scopo di migliorare il servizio offerto dall'ateneo.
Fornendo ad esempio orari, stato di attività del servizio e il numero di posti
disponibili in modo più veloce e accessibile.

Per la relazione vedere il file 'main.pdf'
